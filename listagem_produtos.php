<?php  
	session_start();

	$usuario = $_SESSION['usuario'];

	if(empty($usuario)) {

		header("Location: login.php");
	
	} 

	$login = "dispensa";
	$senha = "2018";
	$id = 1;

	$base_url = $usuario['base_url'];
	$pagina="home";
	$produto = "produto/";
	$logoff = "logoff.php";
	$contato = "contato.php";
	$listagem = "listagem_produtos.php";
	
	include_once "topo.php";

	require_once "classes/ProdutoController.php";

	$obj = new ProdutoController('home');

	$produtos_cadastrados = $obj->listar();
	
?>

<head>
    <link href="util/css/metisMenu.min.css" rel="stylesheet">
    <link href="util/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="util/css/dataTables.responsive.css" rel="stylesheet">
    <link href="util/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div class="container">
	    <div id="wrapper">
	        <div id="page-wrapper">
	            <div class="row">
	                <div class="col-lg-12">
	                    <h1 class="page-header">Listagem de Produtos</h1>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-lg-12">
	                    <div class="panel panel-primary">
	                        <div class="panel-heading">
	                            Produtos
	                        </div>
	                        <div class="panel-body">
	                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
	                                <thead>
	                                    <tr>
	                                        <th>Tipo de Produto</th>
	                                        <th>Categoria</th>
	                                        <th>Ações</th>
	                                    </tr>
	                                </thead>
	                                <tbody>

	                                	<?php foreach ($produtos_cadastrados as $prod): ?>
	                                			 
	                                			<tr class="odd gradeX">
	                                				<td><?= utf8_encode($prod->tipo)?></td>
	                                				<td><?= utf8_encode($prod->descricao)?></td>	
	                                				<td class="center">
											        <a href="produto/atualiza_produtos.php/<?=$prod->produto_id?>" class="btn btn-warning" title="Atualizar">
											          <span class="glyphicon glyphicon-edit"></span>
											        </a>
											        <a href="<?=$prod->produto_id?>" class="btn btn-danger">
											          <span class="glyphicon glyphicon-trash" title="Remover"></span>
											        </a>
		                                        </td>	
	                                			</tr>

                                		<?php endforeach;?>
	                                	
	                                </tbody>
	                            </table>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    
	    </div>		
	</div>
    
    <script src="util/js/metisMenu.min.js"></script>
    <script src="util/js/jquery.dataTables.min.js"></script>
    <script src="util/js/dataTables.bootstrap.min.js"></script>
    <script src="util/js/dataTables.responsive.js"></script>

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</html>