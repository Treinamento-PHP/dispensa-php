<?php 
	
	session_start();

	$usuario = $_SESSION['usuario'];

	if(empty($usuario)) {

		header("Location: login.php");
	
	} 

	$base_url = $usuario['base_url'];
	$nome = $usuario['nome'];

	$ds = DIRECTORY_SEPARATOR;

	require_once "classes{$ds}ProdutoController.php"; 

	$obj = new ProdutoController('home');
	$tipo = $obj->getTipo();
	

?>
	<?php 
		$produto = "produto/";
		$contato = "contato.php";

		$logoff = "logoff.php";
		$listagem = "listagem_produtos.php";
		$tipo = 'home';
		include_once "topo.php" ?>

	<div class="container">
		
		<div class="row">
			<div class="col-md-3">
				<label for="tipo">Digite o Tipo de Produto</label>
				<input type="text" name="tipo" class="form-control" id="tipo" value="" required />
			</div>
			<div class="col-md-3" style="margin-left: -25px;">
				<label for="tipo">Categoria</label>
				<select name="tipo" class="form-control">
					<option value=""></option>					
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5" style="margin-top: 30px;">
				<input type="button" name="cadastro" id="cadastrar" class="btn btn-primary block"  value="Cadastrar" />
			</div>
		</div>

		<script src="util/js/topo.js"></script>
		<script src="util/js/home.js"></script>
		<?php $top = "450px;"; ?>
		<?php include_once "rodape.php" ?>			
	</div>

	
