<?php 

	function getConnection() {

		$conn = null;
		//$user = "eduardo";
		$user = "root";
		//$password = "root";
        $password = "";
		//$dsn = "mysql:host=192.168.64.2;dbname=dispensa";	
		$dsn = "mysql:host=localhost;dbname=dispensa";
		try {
		    $conn = new PDO($dsn, $user, $password);
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8'");
		} catch (PDOException $e) {
		    echo $e->getMessage();
		}

		return $conn;
	}

 ?>
