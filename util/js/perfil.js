$(function() {

	function valida_senha() {

		var senha = $("#senha").val();
		var old = $("#senha_velha").val();
        var usuario = $("#usuario_id").val();

		if(senha == '') {

			alert("A senha é obrigatório");
			$("#senha").focus();
		
		} else if(old == senha) {

			alert("Informe outra senha.");

			$("#senha").val('');
			$("#senha").focus();
	
		} else {
	
			atualiza_usuario(usuario, senha);
			
		}

	}

	function redirect(pagina) {

		window.location.href = pagina + ".php";
	}

	function atualiza_usuario(usuario, senha) {

		$.ajax({
			url: 'classes/perfil.php',
			type: 'post',
			dateType: 'json',
			data:{
				login: usuario,
				password: senha
			},
			success: function(Resp) {

				var resp = JSON.parse(Resp);

				if(resp) {

					alert("Senha atualizada com sucesso!");
					redirect("home");

				} else {

					redirect("home");
				}

			}
		});
	}

	$(document).ready(function(){

		$("#senha").focus();

		$("#atualizar").click(function(){

			valida_senha();

		});

		$("#cancelar").click(function(){

			redirect("home");

		});

	})
})