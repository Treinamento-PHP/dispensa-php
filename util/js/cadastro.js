
$(function() {
	
	function redirect(pagina) {

		window.location.href = pagina + ".php";
	}
	
	function cadastro_usuario(usuario, senha, login) {

		$.ajax({
			url: 'classes/cadastro_usuario.php',
			type: 'post',
			dateType: 'json',
			data:{
				nome: usuario,
				password: senha,
				login: login
			},
			success: function(Resp) {

				var resp = JSON.parse(Resp);

				if(resp) {

					alert("O usúario foi criado com sucesso.");

				}

				redirect('home');
			}
		});
	}

	$(document).ready(function(){

		$("#nome").focus();

		$("#cadastro").click(function(){

			var usuario = $("#nome").val();
			var login = $("#usuario").val();
			var senha = $("#senha").val();

			if(usuario && senha && login) {

				cadastro_usuario(usuario, senha, login);

			} else {

				alert("O campo usúario e senha são obrigatórios.");

				$("#usuario").focus();
			}

		});

		$("#cancelar").click(function(){

			redirect("home");

		});

	});
});



