$(function() {

	function cadastrar_produto() {

		var nome = $("#nome").val();
		var tipo = $("#tipo").val();
		var produto_id = $("#id").val();

		if(nome && tipo) {

			var url = produto_id ? '../../classes/produto.php' : '../classes/produto.php';

			$.ajax({				
				url: url,
				type: 'post',
				dateType: 'json',
				data:{
					nome: nome,
					tipo: tipo,
					id: produto_id
				},
				success: function(Resp) {

					var resp = JSON.parse(Resp);

					if(resp) {
						
						if(produto_id) {

							alert("Produto atualizado com sucesso!");
	
						} else {
	
							alert("Produto cadastrado com sucesso!");
							
						}

					} else {

						alert("Erro ao cadastrar o produto!");
					}

					var pagina = '';

					if(produto_id) {

						pagina = "../../home.php";
					
					} else {

						pagina = "../home.php";
						
					}

					window.location.href = pagina;
				}
			});
		}
	}

	$(document).ready(function(){

		$("#nome").focus();

		$("#cadastrar").click(function(){
			cadastrar_produto();
			
		});
	});
});
