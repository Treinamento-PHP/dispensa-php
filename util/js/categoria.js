$(function() {

	function redirect(pagina) {

		window.location.href = pagina + ".php";
	}

	function cadastrar(categoria) {

		$.ajax({
			url: 'classes/categoria.php',
			type: 'post',
			dateType: 'json',
			data:{
				categoria: categoria
			},
			success: function(Resp) {

				var resp = JSON.parse(Resp);

				if(resp) {

					alert("Categoria cadastrada com sucesso!");

				} else {

					alert("Erro ao tentar cadastrar a Categoria!");
				}

				redirect("home");
			}

		});
		
	}

	$(document).ready(function(){

		$("#categoria").focus();

		$("#cadastrar").click(function(){

			var categoria = $("#categoria").val();

			if(categoria == '') {

				alert("O campo categoria é obrigatório!");
				$("#categoria").focus();
			
			} else {

				cadastrar(categoria);
			
			}
		});
	});
});