<?php 
	
	session_start();

	$usuario = $_SESSION['usuario'];

	if(empty($usuario)) {

		header("Location: login.php");
	
	} 

	$base_url = $usuario['base_url'];
	$nome = $usuario['nome'];
	$pagina="home";

?>
	<?php 
		$produto = "produto/";
		$contato = "contato.php";

		$logoff = "logoff.php";
		$listagem = "listagem_produtos.php";
		$tipo = 'home';
		include_once "topo.php" ?>

	<div class="container">
		<div class="alert alert-success" role="alert" id="msg_topo"> <h3>Bem vindo ao sistema</h3><strong> <?=$nome ?></strong></div>
		<div class="row">
		<div class="col-md5">
			<img src="<?=$base_url?>util/images/piramide-alimentar.jpg" alt="piramide alimentar" style="height: 450px;">
			<blockquote style="margin-top: 70px;">
			  <p>"A saúde não está na forma física, mas na forma de se alimentar".</p>
			  <footer>Autor:<cite title="Source Title">Fábio Ibrahim El Khoury</cite></footer>
			</blockquote>		
		</div>

		<script src="util/js/topo.js"></script>
		<script src="util/js/home.js"></script>

		<?php include_once "rodape.php" ?>			
	</div>

	
