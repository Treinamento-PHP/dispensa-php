<?php 

	include_once "classes/LoginController.php"; 

	$dados = $_POST;

	if($dados) {
		$login = new LoginController($dados['username'], $dados['password']);
		$login->logar();
		
	}
?>

<html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login</title>
    <!-- Bootstrap -->
    <link href="util/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	</head>

	<body>
		<div class="container">
			<form class="form-horizontal" method="post" action="login.php" autocomplete="off">
				<div class="row">
					<div class="panel panel-primary" style="width:500px;margin-left: 350px;margin-top: 70px;"> 
						<div class="panel-heading"> 
							<h3 class="panel-title"><label style="margin-left: 180px;">LOGIN DE ACESSO</label></h3> 
						</div> 
							<div class="panel-body"> 
								<div class="col-xs-12 col-sm-6 col-md-12">
									<div class="control-group">
										<label for="username">Usuário</label>
										<div class="controls">
											<input id="username" name="username" type="text" class="form-control"  placeholder="Digite seu login" required>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-12">
									<div class="control-group" style="margin-top: 10px;">
										<label for="password">Senha</label>
										<div class="controls">
											<input  name="password" type="password" class="form-control" placeholder="Digite sua Senha" required>
										</div>
									</div>								
								</div>
								<div class="col-xs-12 col-sm-6 col-md-12">
									<div class="form-actions" style="margin-top: 20px;">
										<input type="submit" class="btn btn-primary btn-lg btn-block" value="Entrar"/>		                                                    
									</div>																
								</div>
							</div>
						</div> 
					</div>
				</div>
			</form>
		    <?php include_once "rodape.php" ?>
		

        


