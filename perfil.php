<?php  
	session_start();

	$usuario = $_SESSION['usuario'];

	if(empty($usuario)) {

		header("Location: login.php");
	
	} 

	$login = $usuario['login'];
	$senha = $usuario['senha'];
	$id = $usuario['usuario_id'];

	$base_url = $usuario['base_url'];
	$pagina="home";
	$produto = "produto/";
	$logoff = "logoff.php";
	$listagem = "listagem_produtos.php";
	
	include_once "topo.php";

?>

<div class="container">	
	<div class="row">
		<input type="hidden" name="senha_velha" id="senha_velha" value="<?=$senha?>" />
		<input type="hidden" name="usuario_id" id="usuario_id" value="<?=$id?>" />
		<div class="col-md-2">
			<label for="usuario">Usuário</label>
			<input type="text" name="usuario" id="usuario" class="form-control"  value="<?= $login?>" readonly/>
			<label for="senha" style="margin-top: 10px">Senha</label>
			<input type="password" name="" class="form-control" value="<?= $senha?>" readonly/>		
			<label for="senha" style="margin-top: 10px">Nova Senha</label>
			<input type="password" name="senha" id="senha" class="form-control" value=""/>		
		</div>
		<div class="col-md-12">
			<button class="btn btn-danger" id="cancelar" style="margin-top: 20px">Cancelar</button>	
			<button class="btn btn-primary" id="atualizar" style="margin-top: 20px">Atualizar</button>	
		</div>
	</div>
</div>

<script src="util/js/perfil.js"></script>

<?php $top="300px" ?>
<?php include_once "rodape.php"; ?>	

