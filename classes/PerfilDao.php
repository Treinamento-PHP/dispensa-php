<?php 

	$ds = DIRECTORY_SEPARATOR;

	require_once "..{$ds}conexao{$ds}conecta.php";

	class PerfilDao {

		private $conn;

		public function __construct() {

			$this->conn = getConnection();
		}

		public function atualiza($dados) {

			$stmt = $this->conn->prepare('update usuario set senha = :senha where usuario_id = :usuario_id');
			
			$stmt->execute(array(
				':senha'   => $dados['senha'],
			    ':usuario_id' => $dados['usuario_id']
			));
			     
			$result = $stmt->rowCount();

			return $result;
		}

		public function gravar($dados) {

			$stmt = $this->conn->prepare('insert into usuario set senha = :senha, nome = :nome,  login  = :login');
			
			$stmt->execute(array(
				':senha'   => $dados['senha'],
				':nome'   => $dados['nome'],
			    ':login' => $dados['login']
			));
			     
			$result = $stmt->rowCount();

			return $result;
		}

	}

?>