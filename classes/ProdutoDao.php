<?php 

	class ProdutoDao {

		private $conn;

		public function __construct($pagina) {
			
			$ds = DIRECTORY_SEPARATOR;

			if ($pagina == 'home') {

				require_once "conexao{$ds}conecta.php";
			
			} elseif($pagina == 'produto' || $pagina == 'edicao' || $pagina == 'cadastro'){

				require_once "..{$ds}conexao{$ds}conecta.php";
			
			}

			$this->conn = getConnection();
		}

		public function getProdutos() {

			$sql = "
				select c.*, t.descricao	as tipo from tipo t
				inner join categoria c
				on c.categoria_id = t.categoria_id";
			$stm = $this->conn->prepare($sql);

			$stm->execute();

			$produtos = $stm->fetchAll(PDO::FETCH_OBJ);

			return $produtos;
		}

		public function buscaProduto($id) {

			$sql = "
				select p.* ,pt.descricao 
				from produto p 
				inner join produto_tipo pt 
				on p.produto_tipo = pt.tipo_id
				where p.produto_id = ?";

			$stm = $this->conn->prepare($sql);
			$stm->bindValue(1, $id, PDO::PARAM_INT);
			$stm->execute();

			$produtos = $stm->fetchAll(PDO::FETCH_OBJ);

			return $produtos;
		}

		public function getTipo() {

			$sql = "
				select p.* ,pt.descricao 
				from produto p 
				inner join produto_tipo pt 
				on p.produto_tipo = pt.tipo_id
				where p.produto_id = ?";

			$stm = $this->conn->prepare($sql);
			$stm->bindValue(1, $id, PDO::PARAM_INT);
			$stm->execute();

			$tipo = $stm->fetchAll(PDO::FETCH_OBJ);

			return $tipo;

		}

		public function buscaProdutoTipo($id) {

			$sql = "
				select t.tipo_id, t.descricao as tipo 
				from tipo t				
				where t.categoria_id = ?
				order by 2 asc
			";

			$stm = $this->conn->prepare($sql);
			$stm->bindValue(1, $id, PDO::PARAM_INT);
			$stm->execute();

			$produtos = $stm->fetchAll(PDO::FETCH_OBJ);

			return $produtos;
		}

		public function gravar($dados) {
			
			$sql = "INSERT INTO produto (produto_nome, produto_tipo) VALUES (:produto_nome, :produto_tipo)";
			$stm = $this->conn->prepare($sql);
			$stm->execute($dados);
			     
			$result = $stm->rowCount();
			
			return $result;
		}

		public function atualizar($dados) {
			
			$sql = "UPDATE produto SET produto_nome = :produto_nome WHERE produto_tipo = :produto_tipo";
			$stm = $this->conn->prepare($sql);
			$stm->execute($dados);
			     
			$result = $stm->rowCount();
			
			return $result;
		}
	}
 ?>