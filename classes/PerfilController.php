<?php 
	
	require_once "PerfilDao.php";

	class PerfilController {

		private $usuario_id;
		private $senha;
		private $nome;
		private $login;
		private $dao;

		public function __construct($usuario_id, $senha, $nome = null, $login = null) {

			$this->usuario_id = $usuario_id;
			$this->senha = $senha;
			$this->nome = $nome;
			$this->login = $login;

			$this->dao = new PerfilDao();
		}

		public function gravar() {

			$dados = array(
				'login' =>$this->login,
				'nome' =>$this->nome,
				'senha' => md5($this->senha)
			);

			echo json_encode($this->dao->gravar($dados));
		}

		public function alterar() {

			$dados = array(
				'usuario_id' =>$this->usuario_id,
				'senha' => md5($this->senha)
			);

			echo json_encode($this->dao->atualiza($dados));
		}
	}
 ?>