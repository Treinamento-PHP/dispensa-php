<?php 
	
	require_once "../classes/ProdutoController.php";

		$dados = $_POST;

		if($dados) {

			if(empty($dados['id'])) {

				$produto = new ProdutoController('cadastro');
				$resultado = $produto->gravar($dados['nome'],$dados['tipo']);

				echo json_encode($resultado);
			
			} else {

				$produto = new ProdutoController('edicao');
				$resultado = $produto->atualizar($dados['nome'],$dados['tipo']);

				echo json_encode($resultado);

			}
			
			
		}
 ?>