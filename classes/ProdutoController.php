<?php 
	
	require_once "ProdutoDao.php";

	class ProdutoController {
		
		private $dao;

		public function __construct($pagina) {

			$this->dao = new ProdutoDao($pagina);

		}
		
		public function gravar($nome, $tipo) {
			
			$produto = array(
				
				'produto_nome' => $nome,
				'produto_tipo' => $tipo
			);

			$resultado = $this->dao->gravar($produto);

			return $resultado;
		}

		public function atualizar($nome, $tipo) {
			
			$produto = array(
				
				'produto_nome' => $nome,
				'produto_tipo' => $tipo
			);

			$resultado = $this->dao->atualizar($produto);

			return $resultado;
		}

		public function listar() {

			$produtos = $this->dao->getProdutos();

			return $produtos;

		}

		public function getProdutosTipo($id) {

			$produto = $this->dao->buscaProdutoTipo($id);

			return $produto;

		}

		public function getProdutoOne($id) {

			$produto = $this->dao->buscaProduto($id);

			return $produto;

		}

		public function getTipo() {

			//$tipo = $this->dao->getTipo();

			//return $tipo;
		}

		public function debug($dados) {

			echo "<pre>";
			print_r($dados);
			die();

		}
	}

 ?>