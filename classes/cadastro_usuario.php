<?php 
	
	include_once "PerfilController.php";

	$dados = $_POST;

	if($dados) {
		
		$usuario = new PerfilController(0, $dados['password'], $dados['nome'], $dados['login']);
		$usuario->gravar();

	} else {

		header("Location: home.php");
	}

?>