<?php 

	class CategoriaDao {

		private $conn;
		
		public function __construct($pagina) {

			$ds = DIRECTORY_SEPARATOR;

			if($pagina == 'home' || empty($pagina)) {

				require_once "conexao{$ds}conecta.php";
			
			} elseif($pagina == 'categoria') {

				require_once "..{$ds}conexao{$ds}conecta.php";
					
			} else {

				require_once "..{$ds}conexao{$ds}conecta.php";
			}

			$this->conn = getConnection();
		}

		public function gravar($categoria) {
			
			$stmt = $this->conn->prepare('insert into categoria set descricao = :categoria');
			
			$stmt->execute(array(
				':categoria'   => $categoria
			));
			     
			$result = $stmt->rowCount();

			return $result;

		}

		public function getCategoria() {

			$sql = "select * from categoria order by 1 asc";

			$stm = $this->conn->prepare($sql);

			$stm->execute();

			$categoria = $stm->fetchAll(PDO::FETCH_OBJ);

			return $categoria;
		}
	}

 ?>