<?php 
	
	class UsuarioController {

		private $usuario;
		private $senha;

		public function __construct($usuario, $senha) {

			$this->usuario = $usuario;
			$this->senha = $senha;

		}

		public function gravar() {

			$usuario = [
				'usuario' => $this->usuario,
				'senha' => $this->senha,
			];

			$this->debug($usuario);
			
		}

		public function debug($dados) {

			echo "<pre>";
			print_r($dados);
			die();

		}
	}
?>