<?php 
	
	$ds = DIRECTORY_SEPARATOR;

	require_once "conexao{$ds}conecta.php";

	class UsuarioDao {

		private $conn;

		public function __construct() {

			$this->conn = getConnection();
		}
		
		public function getUsuario($usuario, $senha) {

			$password = md5($senha);

			$sql = "select * from usuario where login = ? and senha = ?";
			
			$stm = $this->conn->prepare($sql);
			$stm->bindValue(1, $usuario, PDO::PARAM_STR);
			$stm->bindValue(2, $password, PDO::PARAM_STR);

			$stm->execute();

			$usuario = $stm->fetchAll(PDO::FETCH_OBJ);

			return $usuario;
		}

		
	}



 ?>