<?php 
	
	require_once "UsuarioDao.php";

	class LoginController {

		private $usuario;
		private $senha;

		public function __construct($usuario, $senha) {

			$this->usuario = $usuario;
			$this->senha = $senha;
		}

		public function logar() {						

			$this->valida_usuario();

		}

		public function valida_usuario() {

			$dao = new UsuarioDao();
			$usuario = $dao->getUsuario($this->usuario, $this->senha);

			if(empty($usuario[0]->login) &&  empty($usuario[0]->senha)) {

				header("Location: login.php"); 
			
			} else {

				if($this->usuario == $usuario[0]->login && md5($this->senha) == $usuario[0]->senha) {

					session_start();
					//$this->debug($usuario[0]);
					$usuario = array(
						'usuario_id' => $usuario[0]->usuario_id,
						'login' => $usuario[0]->login,
						'nome' => $usuario[0]->nome,
						'senha' => $usuario[0]->senha,
						'time' => time(),
						'base_url' => '../dispensa/'
					); 

					$_SESSION['usuario'] = $usuario;

					//$this->debug($_SESSION);
					header("Location: home.php");
				
				} else {

					header("Location: login.php");
				}
			}
		}

		public function deslogar() {

			session_start();
			session_destroy();
			unset( $_SESSION );
			

			header("Location: login.php"); 

		}

		public function debug($dados) {

			echo "<pre>";
			print_r($dados);
			die();

		}
	}

 ?>