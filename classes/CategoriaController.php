<?php 
	
	require_once "CategoriaDao.php";

	class CategoriaController {

		private $categoria;
		private $dao;

		public function __construct($categoria = null, $pagina) {

			$this->categoria = $categoria;
			$this->dao = new CategoriaDao($pagina);

		}

		public function gravar() {

			$dados = $this->categoria;

			$result = $this->dao->gravar($dados);

			return $result;
		}

		public function getCategoria() {

			$categorias = $this->dao->getCategoria();

			return $categorias;
		}

	}
 ?>