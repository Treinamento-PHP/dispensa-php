  <?php  
  $path_root = '';
  
  $ds = DIRECTORY_SEPARATOR;

  if (isset($dados)) {
    
    $path_root = "{$ds}";
    $path_root = "..{$ds}..{$ds}produto{$path_root}";

  } elseif(isset($home)) {
  
    $path_root = "{$ds}";
    $path_root = "produto{$path_root}";

  } elseif(isset($produto)) {

    $path_root = "{$ds}";
    $path_root = "{$produto}";

  } 

  if($pagina == 'local') {
  
    $path_root = "..{$ds}";
  
  } 

  require_once "classes/CategoriaController.php";

  $pagina = isset($pagina) ? $pagina : '';

  $obj = new CategoriaController('',$pagina);
  $categorias = $obj->getCategoria();

?>

<style>

	.navbar-nav>li>a {
		background-color: #ffa500;			
	}

	img{
		max-width:100%;
		height:auto;
	}
</style>
	
<nav class="navbar navbar-light" style="background-color: #ffa500;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=$index?>">Dispensa</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?=$contato?>">Contato <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Estoque</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cadastros<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li role="separator" class="divider"></li>
            <li><a href=""><b>Cadastro de Produtos</b></a></li>
            <li role="separator" class="divider"></li>
            <?php if ($categorias): ?>
              <?php foreach ($categorias as $c): ?>
    
                <li><a href="<?=$path_root?>categoria.php/<?= $c->categoria_id ?>"><?= utf8_encode($c->descricao) ?></a></li>
                
              <?php endforeach ?>
            <?php endif ?>
            
            <li role="separator" class="divider"></li>
            <li><a href="categoria.php"><b>Cadastro de Categorias do Produto</b></a></li>
            <li role="separator" class="divider"></li>
            <li role="separator" class="divider"></li>
            <li><a href="tipo.php"><b>Cadastro de Tipo de Produto</b></a></li>
            <li role="separator" class="divider"></li>
          </ul>
        </li>
        <li><a href="<?=$listagem?>">Listagem de Produtos</a></li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Configurações <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/dispensa/perfil.php">Atualizar Perfil</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/dispensa/cadastro_usuario.php">Cadastro de Usúarios</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?=$logoff?>">Sair</a></li>
          </ul>
        </li>
      </ul>
    </div>
</nav>