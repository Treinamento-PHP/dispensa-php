<?php 
	
	session_start();

	$usuario = $_SESSION['usuario'];

	if(empty($usuario)) {
		header("Location: login.php");	
	} 

	$base_url = $usuario['base_url'];

?>