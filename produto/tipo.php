<?php 
	
	$arr = explode("/", $_SERVER['PATH_INFO']);
	
	$tipo = end($arr);

	$ds = DIRECTORY_SEPARATOR;
	$path_root = "..{$ds}";
	$path_root = "{$path_root}";

	$pagina = '';

	switch($tipo) {

		case 1: 
			$pagina = "{$path_root}acougue.php";
			break;
		case 2: 
			$pagina = "{$path_root}hortifruit.php";
			break;
		case 3: 
			$pagina = "{$path_root}limpeza.php";
			break;
		case 4: 
			$pagina = "{$path_root}mantimentos.php";
			break;
		case 5: 
			$pagina = "{$path_root}padaria.php";
			break;
	}

	header("Location: {$pagina}");
 ?>