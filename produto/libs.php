<?php 
	$ds = DIRECTORY_SEPARATOR;

	if (isset($dados)) {

		$path_root = "..{$ds}..{$ds}";
		$path_root = "{$path_root}{$ds}util{$ds}bootstrap{$ds}";
		
	} elseif($pagina == 'categoria' || $pagina == 'local') {

		$path_root = "..{$ds}..{$ds}";
		$path_root = "{$path_root}util{$ds}bootstrap{$ds}";
		
	} 
	
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="<?=$path_root."css{$ds}"?>bootstrap.min.css" rel="stylesheet">

	<script src="<?=$path_root."js{$ds}"?>jquery.min.js" type="text/javascript"></script>
	<script src="<?=$path_root."js{$ds}"?>bootstrap.min.js" type="text/javascript"></script>

	<title>Dispensa</title>

	<style>
		hr{
		  border-color:<?=$color?>; 
		  border-width: 5px;
		  border-style: dashed;
		  box-sizing:border-box;
		  width:100%;  
		}
	</style>
</head>