<?php 
	
	$ds = DIRECTORY_SEPARATOR;

	require_once "logado.php";

	$caminho = 'atualiza';

	require_once "..{$ds}classes{$ds}ProdutoController.php";

	$url = explode("/", $_SERVER['PATH_INFO']);
	
	$produto_id = end($url);

	$obj = new ProdutoController('edicao');

	$dados = $obj->getProdutoOne($produto_id);

	$tipo = $dados[0]->produto_tipo;
	$titulo = $dados[0]->descricao;
	
	switch($tipo) {

		case '1': 
			$color = "#FF0000";
			include_once "acougue.php";
			break;
		case '2': 
			$color = "#32CD32";
			include_once "hortifruit.php";
			break;
		case '3': 
			$color = "#00BFFF";
			include_once "limpeza.php";
			break;
		case '4': 
			$color = "#800080";
			include_once "mantimentos.php";
			break;
		case '5': 
			$color = "#A0522D";
			include_once "padaria.php";
			break;
	}

	include_once "libs.php";
	include_once "produto.php";

 ?>

 