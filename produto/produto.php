<?php 
	$ds = DIRECTORY_SEPARATOR;
	$path_root = "..{$ds}";
	$path_root = "{$path_root}";

	if (isset($dados)) {
		
		$contato = "..{$ds}{$path_root}contato.php";
		$logoff = "..{$ds}{$path_root}logoff.php";
		$listagem = "..{$ds}{$path_root}listagem_produtos.php";
		$index = "..{$ds}{$path_root}home.php";
		include_once "{$path_root}navebar.php";
		
	} else {

		$contato = "{$path_root}contato.php";
		$logoff = "{$path_root}logoff.php";
		$listagem = "{$path_root}listagem_produtos.php";
		$index = "{$path_root}home.php";
		include_once "{$path_root}navebar.php";

	}
	
 ?>

	<div class="container">
		<h1 style="color:<?=$color?>"><?=$titulo?></h1>
		<hr />
		<div class="row">
			<div class="col-md-3">
				<label for="nome">Produto</label>
				<?php $nome = isset($dados[0]->produto_nome) ? utf8_encode($dados[0]->produto_nome) : ''; ?>
				<input type="text" name="nome" class="form-control" id="nome" value="<?=$nome?>" required />
			</div>
			<?php 
				
				if(isset($dados[0]->produto_tipo)) {

					$produto_tipo = $dados[0]->produto_tipo;
					$readonly = 'disabled';
					$descricao = utf8_encode($dados[0]->descricao);

				} else {

					$produto_tipo = '';
					$readonly = '';
					$descricao = '';

				}

				if(isset($produtos)): ?>
				<div class="col-md-3" style="margin-left: -25px;">
					<label for="tipo">Tipo</label>
					<select name="tipo" id="tipo" class="form-control" id="tipo" required <?=$readonly?>>
						<option value="<?=$produto_tipo?>"><?= $descricao ?></option>
						<?php foreach($produtos as $k => $p): ?>
							<option value="<?=$p->tipo_id?>"><?= utf8_encode($p->tipo)?></option>						
						<?php endforeach; ?>
					</select>
				</div>
			<?php endif; ?>
		</div>

		<?php $nome = isset($dados[0]->produto_id) ? "Atualizar" : "Cadastrar" ?>

		<input type="hidden" name="id" id="id" value="<?= isset($dados[0]->produto_id) ? $dados[0]->produto_id : ''; ?>">
		<div class="row">
			<div class="col-md-5" style="margin-top: 30px;">
				<input type="button" name="cadastro" id="cadastrar" class="btn btn-primary block"  value="<?=$nome?>" />
			</div>
		</div>
	</div>
<?php 

	if (isset($caminho)) {
		
		$path_root = "..{$ds}..{$ds}util{$ds}js{$ds}";

	} else {
		
		$path_root = "..{$ds}util{$ds}js{$ds}";

	}

 ?>	
<script type="" src="<?=$path_root?>produto.js"></script>
<?php $top = "500px;"; ?>
<?php include_once "..{$ds}rodape.php" ?>	