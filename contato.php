<?php 
	
	session_start();

	$usuario = $_SESSION['usuario'];

	if(empty($usuario)) {

		header("Location: login.php");
	
	} 

	$base_url = $usuario['base_url'];
	$pagina="home";
?>
	<?php 
		$contato = "contato.php";
		$logoff = "logoff.php";
		$produto = "produto/";
		$listagem = "listagem_produtos.php";

		include_once "topo.php" ?>

	<div class="container">
		<div class="row">
		<div class="col-md5">
			<img src="<?=$base_url?>util/images/contato.jpeg" alt="contato E-mail" style="height: 450px;">	
			<address style="margin-left: 180px;">
				
			</style>
			  <strong>Eduardo Oliveira</strong><br>
			  <a href="mailto:emiranda.dev@gmail.com">emiranda.dev@gmail.com</a>
			</address>		
		</div>

		<script src="util/js/topo.js"></script>
		<script src="util/js/home.js"></script>

		<?php include_once "rodape.php" ?>			
	</div>
