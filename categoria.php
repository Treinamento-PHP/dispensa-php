<?php 
	
	session_start();

	$usuario = $_SESSION['usuario'];

	if(empty($usuario)) {

		header("Location: login.php");
	
	} 

	$base_url = $usuario['base_url'];
	$nome = $usuario['nome'];

?>
	<?php 
		$produto = "produto/";
		$contato = "contato.php";

		$logoff = "logoff.php";
		$listagem = "listagem_produtos.php";
		$tipo = 'home';
		include_once "topo.php" ?>

	<div class="container">
		
		<div class="row">
			<div class="col-md-3">
				<label for="categoria">Digite a Categoria</label>
				<input type="text" name="categoria" class="form-control" id="categoria" value="" required />
			</div>
		</div>
		<div class="row">
			<div class="col-md-5" style="margin-top: 30px;">
				<input type="button" name="cadastro" id="cadastrar" class="btn btn-primary block"  value="Cadastrar" />
			</div>
		</div>

		<script src="util/js/categoria.js"></script>

		<?php $top = "450px;"; ?>
		<?php include_once "rodape.php" ?>			
	</div>

	
