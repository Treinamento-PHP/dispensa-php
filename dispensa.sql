-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03-Out-2018 às 19:56
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dispensa`
--
CREATE DATABASE IF NOT EXISTS `dispensa` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `dispensa`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--
-- Criação: 03-Out-2018 às 16:18
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `categoria_id` int(11) NOT NULL,
  `descricao` varchar(100) COLLATE utf8_bin NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` VALUES
(1, 'Açougue', '2018-10-03 16:22:17'),
(2, 'HortFruit', '2018-10-03 16:22:17'),
(3, 'Limpeza', '2018-10-03 16:22:17'),
(4, 'Mantimentos', '2018-10-03 16:22:17'),
(5, 'Padaria', '2018-10-03 16:22:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--
-- Criação: 03-Out-2018 às 16:51
--

DROP TABLE IF EXISTS `produto`;
CREATE TABLE `produto` (
  `produto_id` int(11) NOT NULL,
  `nome` varchar(100) COLLATE utf8_bin NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtoold`
--
-- Criação: 03-Out-2018 às 16:46
--

DROP TABLE IF EXISTS `produtoold`;
CREATE TABLE `produtoold` (
  `produto_id` int(11) NOT NULL,
  `produto_nome` varchar(100) NOT NULL,
  `produto_tipo` int(11) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtoold`
--

INSERT INTO `produtoold` VALUES
(1, 'Carne de Boi', 1, '2018-09-29 12:20:28'),
(2, 'Carne de Frango', 1, '2018-09-29 12:20:28'),
(3, 'Carne de Galinha', 1, '2018-09-29 12:20:28'),
(4, 'Outros', 1, '2018-09-29 12:20:28'),
(5, 'Carne de Peixe', 1, '2018-09-29 12:20:28'),
(6, 'Carne de Peru', 1, '2018-09-29 12:20:28'),
(7, 'Carne de Porco', 1, '2018-09-29 12:20:28'),
(8, 'Carne de Vaca', 1, '2018-09-29 12:20:28'),
(9, 'Alface crespa', 2, '2018-10-03 14:40:20'),
(15, 'Banheiro', 3, '2018-09-29 12:24:50'),
(16, 'Cozinha', 3, '2018-09-29 12:24:50'),
(17, 'Lavanderia', 3, '2018-09-29 12:24:50'),
(18, 'Outros', 3, '2018-09-29 12:24:50'),
(19, 'Dispensa', 4, '2018-09-29 12:25:58'),
(20, 'Geladeira', 4, '2018-09-29 12:25:58'),
(21, 'Outros', 4, '2018-09-29 12:25:58'),
(22, 'Dispensa', 5, '2018-09-29 12:29:23'),
(23, 'Geladeira', 5, '2018-09-29 12:29:23'),
(24, 'Outros', 5, '2018-09-29 12:29:23'),
(38, 'Cereais', 2, '2018-10-03 14:50:39'),
(39, 'Frutas', 2, '2018-10-03 14:50:39'),
(40, 'Grãos', 2, '2018-10-03 14:50:39'),
(41, 'Legumes', 2, '2018-10-03 14:50:39'),
(42, 'Saladas', 2, '2018-10-03 14:50:39'),
(43, 'Outros', 2, '2018-10-03 14:50:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo`
--
-- Criação: 03-Out-2018 às 16:32
--

DROP TABLE IF EXISTS `tipo`;
CREATE TABLE `tipo` (
  `tipo_id` int(11) NOT NULL,
  `descricao` varchar(200) COLLATE utf8_bin NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `tipo`
--

INSERT INTO `tipo` VALUES
(1, 'Carne de Boi', 1, '2018-10-03 16:42:21'),
(2, 'Carne de Frango', 1, '2018-10-03 16:42:21'),
(3, 'Carne de Galinha', 1, '2018-10-03 16:42:21'),
(4, 'Outros', 1, '2018-10-03 16:42:21'),
(5, 'Carne de Peixe', 1, '2018-10-03 16:42:21'),
(6, 'Carne de Peru', 1, '2018-10-03 16:42:21'),
(7, 'Carne de Porco', 1, '2018-10-03 16:42:21'),
(8, 'Carne de Vaca', 1, '2018-10-03 16:42:21'),
(9, 'Cereais', 2, '2018-10-03 16:42:21'),
(10, 'Frutas', 2, '2018-10-03 16:42:21'),
(11, 'Grãos', 2, '2018-10-03 16:42:21'),
(12, 'Legumes', 2, '2018-10-03 16:42:21'),
(13, 'Saladas', 2, '2018-10-03 16:42:21'),
(14, 'Outros', 2, '2018-10-03 16:42:21'),
(15, 'Raízes', 2, '2018-10-03 16:42:21'),
(16, 'Vegetais', 2, '2018-10-03 16:42:21'),
(17, 'Banheiro', 3, '2018-10-03 16:42:21'),
(18, 'Cozinha', 3, '2018-10-03 16:42:21'),
(19, 'Lavanderia', 3, '2018-10-03 16:42:21'),
(20, 'Outros', 3, '2018-10-03 16:42:21'),
(21, 'Dispensa', 4, '2018-10-03 16:42:21'),
(22, 'Geladeira', 4, '2018-10-03 16:42:21'),
(23, 'Outros', 4, '2018-10-03 16:42:21'),
(24, 'Dispensa', 5, '2018-10-03 16:42:21'),
(25, 'Geladeira', 5, '2018-10-03 16:42:21'),
(26, 'Outros', 5, '2018-10-03 16:42:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--
-- Criação: 01-Out-2018 às 12:52
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `login` varchar(50) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `acesso` enum('A','U','I') NOT NULL DEFAULT 'U',
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` VALUES
(1, 'Eduardo Miranda de Oliveira', 'eduardo.oliveira', '202cb962ac59075b964b07152d234b70', 'A', '2018-09-29 09:34:54'),
(2, 'Jucimara Santos de Oliveira', 'jucimara.oliveira', '202cb962ac59075b964b07152d234b70', 'U', '2018-09-29 11:05:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`categoria_id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`produto_id`),
  ADD KEY `tipo_id` (`tipo_id`),
  ADD KEY `categoria_id` (`categoria_id`);

--
-- Indexes for table `produtoold`
--
ALTER TABLE `produtoold`
  ADD PRIMARY KEY (`produto_id`),
  ADD KEY `idx_produto_produto_tipo` (`produto_tipo`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`tipo_id`),
  ADD KEY `categoria_id` (`categoria_id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `categoria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `produto_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produtoold`
--
ALTER TABLE `produtoold`
  MODIFY `produto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `tipo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`tipo_id`) REFERENCES `tipo` (`tipo_id`),
  ADD CONSTRAINT `produto_ibfk_2` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`categoria_id`);

--
-- Limitadores para a tabela `tipo`
--
ALTER TABLE `tipo`
  ADD CONSTRAINT `tipo_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`categoria_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


--
-- Metadata
--
USE `phpmyadmin`;

--
-- Metadata for table categoria
--
-- Error reading data for table phpmyadmin.pma__column_info: #1100 - Tabela 'pma__column_info' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__table_uiprefs: #1100 - Tabela 'pma__table_uiprefs' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__tracking: #1100 - Tabela 'pma__tracking' não foi travada com LOCK TABLES

--
-- Metadata for table produto
--
-- Error reading data for table phpmyadmin.pma__column_info: #1100 - Tabela 'pma__column_info' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__table_uiprefs: #1100 - Tabela 'pma__table_uiprefs' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__tracking: #1100 - Tabela 'pma__tracking' não foi travada com LOCK TABLES

--
-- Metadata for table produtoold
--
-- Error reading data for table phpmyadmin.pma__column_info: #1100 - Tabela 'pma__column_info' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__table_uiprefs: #1100 - Tabela 'pma__table_uiprefs' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__tracking: #1100 - Tabela 'pma__tracking' não foi travada com LOCK TABLES

--
-- Metadata for table tipo
--
-- Error reading data for table phpmyadmin.pma__column_info: #1100 - Tabela 'pma__column_info' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__table_uiprefs: #1100 - Tabela 'pma__table_uiprefs' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__tracking: #1100 - Tabela 'pma__tracking' não foi travada com LOCK TABLES

--
-- Metadata for table usuario
--
-- Error reading data for table phpmyadmin.pma__column_info: #1100 - Tabela 'pma__column_info' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__table_uiprefs: #1100 - Tabela 'pma__table_uiprefs' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__tracking: #1100 - Tabela 'pma__tracking' não foi travada com LOCK TABLES

--
-- Metadata for database dispensa
--
-- Error reading data for table phpmyadmin.pma__bookmark: #1100 - Tabela 'pma__bookmark' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__relation: #1100 - Tabela 'pma__relation' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__savedsearches: #1100 - Tabela 'pma__savedsearches' não foi travada com LOCK TABLES
-- Error reading data for table phpmyadmin.pma__central_columns: #1100 - Tabela 'pma__central_columns' não foi travada com LOCK TABLES
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
