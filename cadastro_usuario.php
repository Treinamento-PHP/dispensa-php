<?php  
	session_start();

	$usuario = $_SESSION['usuario'];

	if(empty($usuario)) {

		header("Location: login.php");
	
	} 

	$base_url = $usuario['base_url'];
	$pagina="home";
	$contato = "contato.php";
	$logoff = "logoff.php";
	$produto = "produto/";
	$listagem = "../listagem_produtos.php";
	
	include_once "topo.php";

?>

<div class="container">	
	<div class="row">
		<div class="col-md-3">
			<label for="nome">Nome</label>
			<input type="text" name="nome" id="nome" class="form-control"  value=""/>			
		</div>
		<div class="col-md-12"></div>
		<div class="col-md-3" style="margin-top: 10px">
			<label for="usuario">Login</label>
			<input type="text" name="usuario" id="usuario" class="form-control"  value=""/>					
			<label for="senha" style="margin-top: 10px">Senha</label>
			<input type="password" name="senha" id="senha" class="form-control" value=""/>		
		</div>
		<div class="col-md-12">
			<button class="btn btn-danger" id="cancelar" style="margin-top: 20px">Cancelar</button>	
			<button class="btn btn-primary" id="cadastro" style="margin-top: 20px">Cadastrar</button>	
		</div>
	</div>
</div>

<script src="util/js/cadastro.js"></script>

<?php $top="300px" ?>
<?php include_once "rodape.php"; ?>	

